package com.demo.dctranywhere.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ActivityInteractionModel : ViewModel() {
    private val isFragmentChange = MutableLiveData<Boolean>()
    fun changeFragment() {
        isFragmentChange.value = true
    }

    val getFragment: LiveData<Boolean>
        get() = isFragmentChange
}