package com.demo.dctranywhere.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.demo.dctranywhere.model.apiresponse.RetrofitResponse
import com.demo.dctranywhere.model.network.RetrofitApiHelper
import com.demo.dctranywhere.model.network.RetrofitBuilder
import kotlinx.coroutines.Dispatchers

class ImageDataViewModel : ViewModel() {

    private val retrofitApiHelper: RetrofitApiHelper =
        RetrofitApiHelper(RetrofitBuilder.retrofitApiInterface)

    fun getImageData(offset: Int, pageLimit: Int) =
        liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
            try {
                val response = retrofitApiHelper.getImageData(offset, pageLimit)
                if (response.isSuccessful) {
                    emit(RetrofitResponse.successResponse(response.body()))
                } else {
                    emit(
                        RetrofitResponse.failureResponse(
                            null,
                            response.errorBody().toString()
                        )
                    )
                }
            } catch (exception: Exception) {
                emit(RetrofitResponse.failureResponse(null, exception.message.toString()))
                Log.e("exception ", "" + exception.message)
            }
        }


}