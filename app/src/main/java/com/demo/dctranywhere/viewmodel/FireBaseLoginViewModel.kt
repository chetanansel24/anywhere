package com.demo.dctranywhere.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.demo.dctranywhere.model.firebase.FireBaseResponse
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await

class FireBaseLoginViewModel : ViewModel() {

    fun doLoginUser(emailId: String, password: String) =
        liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
            try {
                val response = doLogin(emailId, password)
                if (response != null) {
                    emit(FireBaseResponse.successResponse(response))
                } else {
                    emit(
                        FireBaseResponse.failureResponse(
                            null,
                            "Unable to Login"
                        )
                    )
                }
            } catch (exception: Exception) {
                emit(
                    FireBaseResponse.failureResponse(
                        null,
                        exception.message.toString()
                    )
                )
                Log.e("exception ", "" + exception.message)
            }
        }

    private suspend fun doLogin(emailId: String, password: String): AuthResult? {
        return try {
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(emailId, password)
                .await()
        } catch (e: Exception) {
            null
        }
    }
}
