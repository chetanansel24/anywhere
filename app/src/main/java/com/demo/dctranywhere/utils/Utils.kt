package com.demo.dctranywhere.utils

import android.text.TextUtils

class Utils {

    companion object {

        fun isFieldsEmpty(name: String?, emailId: String?, password: String?): Boolean {
            return TextUtils.isEmpty(name) && TextUtils.isEmpty(emailId) &&
                    TextUtils.isEmpty(password)
        }

        fun isLoginEmpty(name: String?, emailId: String?): Boolean {
            return TextUtils.isEmpty(name) && TextUtils.isEmpty(emailId)
        }

        const val PREF_KEY = "USER_PREF_KEY"
    }
}