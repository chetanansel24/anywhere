package com.demo.dctranywhere.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.demo.dctranywhere.R
import com.demo.dctranywhere.model.firebase.FireBaseResponse
import com.demo.dctranywhere.ui.activities.MainActivity
import com.demo.dctranywhere.utils.Utils
import com.demo.dctranywhere.viewmodel.ActivityInteractionModel
import com.demo.dctranywhere.viewmodel.FireBaseLoginViewModel
import com.google.android.material.snackbar.Snackbar

class LoginFragment : Fragment() {

    @BindView(R.id.editTextEmail)
    lateinit var mEmailId: EditText

    @BindView(R.id.editTextPassword)
    lateinit var mPassword: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.login_screen, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    @OnClick(R.id.registerButton)
    fun register() {
        ViewModelProvider(requireActivity()).get(ActivityInteractionModel::class.java)
            .changeFragment()
    }

    @OnClick(R.id.loginButton)
    fun login() {
        val emailId = mEmailId.text.toString()
        val password = mPassword.text.toString()

        if (Utils.isLoginEmpty(emailId, password)) {
            Snackbar.make(mEmailId, getString(R.string.enter_all_fields), Snackbar.LENGTH_SHORT)
                .show()
            return
        }
        val fireBaseModel: FireBaseLoginViewModel = ViewModelProvider(requireActivity()).get(
            FireBaseLoginViewModel::class.java
        )
        fireBaseModel.doLoginUser( emailId, password).observe(viewLifecycleOwner, Observer {
            when (it.responseType) {
                FireBaseResponse.ResponseType.SUCCESS -> {
                    val emailID =  it.response!!.user!!.email
                    val sharedPref = activity?.getSharedPreferences(
                        getString(R.string.pref_key), Context.MODE_PRIVATE)
                    val editor = sharedPref!!.edit()
                    editor.putString(Utils.PREF_KEY, emailID)
                    editor.apply()
                    startActivity(Intent(requireActivity(), MainActivity::class.java))
                    activity!!.finish()
                }
                FireBaseResponse.ResponseType.FAILURE -> {
                    Log.e(LoginFragment::class.simpleName, "" + it.errorMessage)
                    Snackbar.make(mEmailId, ""+it.errorMessage, Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }


}