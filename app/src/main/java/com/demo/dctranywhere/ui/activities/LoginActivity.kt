package com.demo.dctranywhere.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.demo.dctranywhere.R
import com.demo.dctranywhere.ui.login.LoginFragment
import com.demo.dctranywhere.ui.signup.SignUpFragment
import com.demo.dctranywhere.viewmodel.ActivityInteractionModel

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance())
                .commit()
        }

        val detailViewModel: ActivityInteractionModel = ViewModelProvider(this).get(
            ActivityInteractionModel::class.java
        )
        detailViewModel.getFragment.observe(this, Observer {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SignUpFragment.newInstance())
                .addToBackStack(null)
                .commit()
        })
    }


}