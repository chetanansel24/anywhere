package com.demo.dctranywhere.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.demo.dctranywhere.R
import com.demo.dctranywhere.utils.Utils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        nextState()
    }

    private fun nextState() {
        finish()
        val userEmailId = getSharedPreferences(
                getString(R.string.pref_key), Context.MODE_PRIVATE)!!.getString(Utils.PREF_KEY, "")
        if (!TextUtils.isEmpty(userEmailId)) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}