package com.demo.dctranywhere.ui.signup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.demo.dctranywhere.R
import com.demo.dctranywhere.model.firebase.FireBaseResponse
import com.demo.dctranywhere.ui.activities.MainActivity
import com.demo.dctranywhere.ui.login.LoginFragment
import com.demo.dctranywhere.utils.Utils
import com.demo.dctranywhere.utils.Utils.Companion.PREF_KEY
import com.demo.dctranywhere.viewmodel.FireBaseSignUpViewModel
import com.google.android.material.snackbar.Snackbar


class SignUpFragment : Fragment() {

    @BindView(R.id.editTextEmail)
    lateinit var mEmailId: EditText

    @BindView(R.id.editTextPassword)
    lateinit var mPassword: EditText

    @BindView(R.id.editTextName)
    lateinit var mName: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.register_screen, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    @OnClick(R.id.signUpButton)
    fun login() {
        val emailId = mEmailId.text.toString()
        val password = mPassword.text.toString()
        val name = mName.text.toString()
        if (Utils.isFieldsEmpty(emailId, password, name)) {
            Snackbar.make(mEmailId, getString(R.string.enter_all_fields), Snackbar.LENGTH_SHORT)
                .show()
            return
        }
        val fireBaseModel: FireBaseSignUpViewModel = ViewModelProvider(requireActivity()).get(
            FireBaseSignUpViewModel::class.java
        )
        fireBaseModel.doSignUpUser(emailId, password)
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                when (it.responseType) {
                    FireBaseResponse.ResponseType.SUCCESS -> {
                        val sharedPref = activity?.getSharedPreferences(
                            getString(R.string.pref_key), Context.MODE_PRIVATE)
                        val editor = sharedPref!!.edit()
                        editor.putString(PREF_KEY, it.response!!.user!!.email)
                        editor.apply()
                        startActivity(Intent(requireActivity(), MainActivity::class.java))
                        activity!!.finish()
                    }
                    FireBaseResponse.ResponseType.FAILURE -> {
                        Log.e(LoginFragment::class.simpleName, "" + it.errorMessage)
                        Snackbar.make(mEmailId, ""+it.errorMessage, Snackbar.LENGTH_SHORT)
                            .show()
                    }
                }
            })
    }

    companion object {
        fun newInstance(): SignUpFragment {
            return SignUpFragment()
        }
    }

}