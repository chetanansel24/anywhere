package com.demo.dctranywhere.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.demo.dctranywhere.R
import com.demo.dctranywhere.model.apiresponse.RetrofitResponse
import com.demo.dctranywhere.ui.adapter.ImageDataRecycleViewAdapter
import com.demo.dctranywhere.viewmodel.ImageDataViewModel


class HomeFragment : Fragment(), ImageDataRecycleViewAdapter.OnImageItemClickListener {

    @BindView(R.id.recycle_item)
    lateinit var imageDataRecycleView: RecyclerView

    @BindView(R.id.swipe_refresh)
    lateinit var swipeRefreshView: SwipeRefreshLayout
    private lateinit var homeViewModel: ImageDataViewModel
    private var imageDataRecycleViewAdapter: ImageDataRecycleViewAdapter? = null
    private var isLoadMore: Boolean = false
    private var offsetCount: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        homeViewModel = ViewModelProvider(this).get(ImageDataViewModel::class.java)
        val root = inflater.inflate(R.layout.image_screen, container, false)
        ButterKnife.bind(this, root)
        swipeRefreshView.setOnRefreshListener {
            setSpan(19)
            getImageData(10, 19)
        }
        imageDataRecycleViewAdapter = ImageDataRecycleViewAdapter(this)
        imageDataRecycleView.adapter = imageDataRecycleViewAdapter
        setSpan(20)
        getImageData(10, 20)
        return root
    }

    private fun getImageData(offset: Int, pageLimit: Int) {
        homeViewModel.getImageData(offset, pageLimit).observe(viewLifecycleOwner,
            Observer {
                when (it.responseType) {
                    RetrofitResponse.ResponseType.SUCCESS -> {
                        val usersResponse = it.response!!.imageResponse.userList
                        isLoadMore = it.response.imageResponse.hasMore
                        offsetCount = offset + 10
                        imageDataRecycleViewAdapter!!.setImageDataList(usersResponse)
                        swipeRefreshView.isRefreshing = false
                    }
                    RetrofitResponse.ResponseType.FAILURE -> {
                        Log.e(HomeFragment::class.simpleName, "" + it.errorMessage)
                        swipeRefreshView.isRefreshing = false
                    }
                }
            })
    }

    override fun onItemClick(view: View?, position: Int) {
        Log.d(HomeFragment::class.simpleName, "Item Click $position")
    }

    private fun setSpan(pageLimit: Int) {
        val gridLayoutManager = GridLayoutManager(activity, 2)
        imageDataRecycleView.layoutManager = gridLayoutManager
        if (pageLimit % 2 != 0) {
            gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == 0) 2 else 1
                }
            }
        }
    }
}