package com.demo.dctranywhere.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.demo.dctranywhere.R
import com.demo.dctranywhere.model.apiresponse.UsersResponse
import javax.inject.Inject


class ImageDataRecycleViewAdapter @Inject constructor(var mClickListener: OnImageItemClickListener?) :
    RecyclerView.Adapter<ImageDataRecycleViewAdapter.ViewHolder>() {
    private var imageDataList: List<UsersResponse>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.image_item, parent, false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageResponse = imageDataList!![position]
        Glide.with(holder.itemView.context).load(imageResponse.thumbNailUrl)
            .placeholder(R.drawable.ic_baseline_find_replace_24)
            .into(holder.thumbnailImageView)
    }

    override fun getItemCount(): Int {
        return imageDataList!!.size
    }

    fun setImageDataList(imageList: List<UsersResponse>) {
        imageDataList = imageList
        notifyDataSetChanged()
    }

    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val thumbnailImageView: ImageView = itemView.findViewById(R.id.image_data)
        override fun onClick(view: View) {
            if (mClickListener != null) {
                mClickListener!!.onItemClick(view, adapterPosition)
            }
        }

        init {
            itemView.setOnClickListener(this)
        }
    }

    interface OnImageItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}