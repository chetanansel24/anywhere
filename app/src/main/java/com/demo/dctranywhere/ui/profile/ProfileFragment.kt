package com.demo.dctranywhere.ui.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.demo.dctranywhere.R
import com.demo.dctranywhere.ui.activities.SplashActivity
import com.demo.dctranywhere.utils.Utils
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment : Fragment() {


    @BindView(R.id.user_emailid)
    lateinit var userEmailIdTextView: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.profile_dashboard, container, false)
        ButterKnife.bind(this, root)
        val userEmailId = activity?.getSharedPreferences(
            getString(R.string.pref_key), Context.MODE_PRIVATE
        )!!.getString(Utils.PREF_KEY, "")
        userEmailIdTextView.text = userEmailId
        return root
    }

    @OnClick(R.id.log_out)
    fun onLogOutClick() {
        FirebaseAuth.getInstance().signOut()
        val sharedPref = activity?.getSharedPreferences(
            getString(R.string.pref_key), Context.MODE_PRIVATE
        )
        val editor = sharedPref!!.edit()
        editor.putString(Utils.PREF_KEY, "")
        editor.apply()
        startActivity(Intent(requireActivity(), SplashActivity::class.java))
        activity!!.finish()
    }
}