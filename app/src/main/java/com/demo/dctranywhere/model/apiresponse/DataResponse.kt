package com.demo.dctranywhere.model.apiresponse

import com.google.gson.annotations.SerializedName

data class DataResponse(@SerializedName("data") val imageResponse: ImageResponse)