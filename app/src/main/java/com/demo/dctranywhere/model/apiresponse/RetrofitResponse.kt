package com.demo.dctranywhere.model.apiresponse


data class RetrofitResponse<out T>(
    val responseType: ResponseType, val response: T?,
    val errorMessage: String?
) {
    companion object {
        fun <T> successResponse(responseData: T): RetrofitResponse<T> =
            RetrofitResponse(
                responseType = ResponseType.SUCCESS, response = responseData,
                errorMessage = null
            )

        fun <T> failureResponse(responseData: T?, message: String): RetrofitResponse<T> =
            RetrofitResponse(
                responseType = ResponseType.FAILURE, response =
                responseData, errorMessage = message
            )
    }

    enum class ResponseType {
        SUCCESS, FAILURE
    }
}