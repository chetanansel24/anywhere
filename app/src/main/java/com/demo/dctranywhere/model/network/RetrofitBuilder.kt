package com.demo.dctranywhere.model.network

import com.demo.dctranywhere.model.network.NetworkUtil.Companion.TIME_OUT
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitBuilder {

    private fun initRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NetworkUtil.NETWORK_BASE_URL)
            .client(initOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun initOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .build()
            chain.proceed(request)
        }
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
        return httpClient.build()
    }

    val retrofitApiInterface: RetrofitApiInterface =
        initRetrofit().create(RetrofitApiInterface::class.java)
}