package com.demo.dctranywhere.model.firebase

data class FireBaseResponse<out T>(
    val responseType: ResponseType, val response: T?,
    val errorMessage: String?
) {
    companion object {
        fun <T> successResponse(responseData: T): FireBaseResponse<T> =
            FireBaseResponse(
                responseType = ResponseType.SUCCESS, response = responseData,
                errorMessage = null
            )

        fun <T> failureResponse(responseData: T?, message: String): FireBaseResponse<T> =
            FireBaseResponse(
                responseType = ResponseType.FAILURE, response =
                responseData, errorMessage = message
            )
    }

    enum class ResponseType {
        SUCCESS, FAILURE
    }
}