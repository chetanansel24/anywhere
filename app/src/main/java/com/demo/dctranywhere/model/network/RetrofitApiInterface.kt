package com.demo.dctranywhere.model.network

import com.demo.dctranywhere.model.apiresponse.DataResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitApiInterface {

    @GET("raw/diBi6FM2")
    suspend fun getDataList(): Response<DataResponse>
}