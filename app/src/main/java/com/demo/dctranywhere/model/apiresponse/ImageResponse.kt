package com.demo.dctranywhere.model.apiresponse

import com.google.gson.annotations.SerializedName

data class ImageResponse(@SerializedName("has_more") val hasMore :Boolean,
                                            @SerializedName("users")
                                            val userList : List<UsersResponse>)
