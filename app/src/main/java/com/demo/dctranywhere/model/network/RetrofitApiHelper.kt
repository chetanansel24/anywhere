package com.demo.dctranywhere.model.network

class RetrofitApiHelper(private val retrofitApiInterface: RetrofitApiInterface) {

    suspend fun getImageData(offset : Int, pageLimit :Int) =
        retrofitApiInterface.getDataList()
}