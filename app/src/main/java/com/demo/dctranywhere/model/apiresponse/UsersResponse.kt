package com.demo.dctranywhere.model.apiresponse

import com.google.gson.annotations.SerializedName

data class UsersResponse(
    @SerializedName("image") val thumbNailUrl: String,
    @SerializedName("name") val userName: String)